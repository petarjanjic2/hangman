import random
import time

#shuffle words and choose last word
rijeci="lasta pas lav slon jelen ajkula nosorog orao rak zebra magarac buba komarac konj krokodil".upper().split()
random.shuffle(rijeci)
slucajna_rijec=rijeci.pop()

#max 6 mistakes and 6 grafics
broj_gresaka=[
'''                        _____ 
                         |   | 
                             | 
                             | 
                             | 
                             | 
                     ________|_''',
'''                        _____ 
                         |   | 
                         O   | 
                             | 
                             | 
                             | 
                     ________|_''',
'''                        _____ 
                         |   | 
                         O   | 
                         |   | 
                             | 
                             | 
                     ________|_''',
'''                        _____ 
                         |   | 
                         O   | 
                        /|   | 
                             | 
                             | 
                     ________|_''',
'''                        _____ 
                         |   | 
                         O   | 
                        /|\  | 
                             | 
                             | 
                     ________|_''',
'''                        _____ 
                         |   | 
                         O   | 
                        /|\  | 
                        /    | 
                             | 
                     ________|_''',
'''                        _____ 
                         |   | 
                         O   | 
                        /|\  | 
                        / \  | 
                             | 
                     ________|_''']

#a collection of guessed and missed letters

pogodjeno = []
promaseno = []

#clear screen
def clear():
        for i in range(30):
                print ('\n')
                
clear()

#title
print(" _  _     ____    ____    ___      __      __        __ ")
print("( \/ )   (_  _)  ( ___)  / __)    /__\    (  )      /__\ ")
print(" \  /   .-_)(     )__)   \__ \   /(__)\    )(__    /(__)\ ")
print("  \/    \____)   (____)  (___/  (__)(__)  (____)  (__)(__)\n")
#print("debug: %s" %slucajna_rijec )

#guess letter or print lines
def crtaj_linije():
    print(broj_gresaka[len(promaseno)])
    for i in slucajna_rijec:
        if i in pogodjeno:
            print(i, end=" ")
        else:
            print("__", end=" ")
    print("\n\n")
    
#print missed letters
    print("promašena slova: ")
    for i in promaseno:
        print(i, end=" ")
        
#print conditions for exit or hint
    print("\n\nza pomoc umjesto slova unesite <?>")
    print("za izlaz umjesto slova unesite <$>")
    print("*************************************")
    
#conditions for input letter, not 2 letters or number
def korisnikovi_pogoci():
    while True:
        pogodak=input("Pogodi slovo: ").upper()
        clear()
        if pogodak in pogodjeno or pogodak in promaseno:
            print("*Već ste pogodili to slovo, pogađaj ponovo*")
        elif pogodak.isnumeric():
            print("*Unesi slovo a ne broj*")
        elif len(pogodak)>1:
            print("*Unesi jedno slovo*")
        elif len(pogodak) == 0:
            print("*Unesite slovo*")

#conditions for hint or exit
        elif pogodak == "?":
            hint()
            break
        elif pogodak =="$":
            time.sleep(1)
            exit()
            break
        
#append letters in colection
        elif pogodak in slucajna_rijec:
            pogodjeno.append(pogodak)
        else:
            promaseno.append(pogodak)
        break

#check win
def provjeri_pobjedu():
    if len(promaseno)>5:
        return "gubis"
    for i in slucajna_rijec:
        if i not in pogodjeno:
            return "nema pobjede"
    return "pobjeda"

#hint. show random letter in word
def hint():
   
    while True:
        slova=list(slucajna_rijec)
        randNum=random.randint(0,len(slova)-1)
        hintSlovo=slova[randNum]

        if hintSlovo not in pogodjeno:
            pogodjeno.append(hintSlovo)
            break

    
#loop for next letter
while True:
    crtaj_linije()
    korisnikovi_pogoci()
    uslovi_pobjede=provjeri_pobjedu()
    
#conditions for victory
    if uslovi_pobjede=="gubis":
        print(broj_gresaka[6])
        print("*********************************************************\n")
        print("GAME OVER!!! PRAVA RIJEČ JE BILA *** %s ****" % slucajna_rijec)
        print("\n*********************************************************")
        break
        
    elif uslovi_pobjede=="pobjeda":
        print(broj_gresaka[len(promaseno)])
        print("*********************************************************\n")
        print("ČESTITAM!!! PRAVA RIJEČ JE BILA *** %s ****" % slucajna_rijec)
        print("\n*********************************************************")
        break

time.sleep(15)
